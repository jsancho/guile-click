#!/usr/bin/guile-3.0 --no-auto-compile
-*- scheme -*-
!#
;;; Click --- Command Line Interface Creation Kit for GNU Guile
;;; Copyright © 2021 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of Click.
;;;
;;; Click is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Click is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Click.  If not, see <http://www.gnu.org/licenses/>.


(use-modules (click))


(define initdb
  (command
   #:name "initdb"
   #:help "Init database."
   #:procedure (lambda ()
                 (format #t "Initialized the database"))))

(define dropdb
  (command
   #:name "dropdb"
   #:help "Drop database."
   #:procedure (lambda ()
                 (format #t "Dropped the database"))))

(define cli
  (group
   #:help "Database Tools"
   #:commands `(,initdb ,dropdb)))

(cli (command-line))
