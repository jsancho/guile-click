;;; Click --- Command Line Interface Creation Kit for GNU Guile
;;; Copyright © 2021 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of Click.
;;;
;;; Click is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Click is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Click.  If not, see <http://www.gnu.org/licenses/>.


(define-module (click)
  #:use-module (srfi srfi-1)
  #:use-module (click args)
  #:use-module (click command)
  #:use-module (click constant)
  #:use-module (click help)
  #:use-module (click util)
  #:export (command
            group))


(define* (command #:key (name #f) (option-spec '()) (help "") (procedure #f))
  "Define a new command for the procedure"
  (group #:name name
         #:option-spec option-spec
         #:help help
         #:procedure procedure))


(define* (group #:key (name #f) (option-spec '()) (help "") (procedure #f) (commands '()))
  "Define a new group with a list of commands associated"
  (let* ((click-option-spec (append option-spec (list HELP_OPTION)))
         (new-command (make-command name click-option-spec help procedure commands)))
    (let ((click-manager
           (lambda (args)
             (let ((values (parse-args args click-option-spec)))
               ;; Call current command
               (cond ((option-ref values 'help #f)
                      (let ((program-name (car args)))
                        (display-help program-name new-command)))
                     (else
                      (when procedure
                        (apply procedure (map cdr (get-values option-spec values))))
                      ;; Call nested command (if exists)
                      (call-nested-command commands values)))))))
               
      (set-command-click-manager! new-command click-manager)
      new-command)))


(define (call-nested-command commands values)
  (let ((next-command-args (cdar values)))
    (when (not (null? next-command-args))
      (let* ((next-command-name (car next-command-args))
             (next-command (find (lambda (command) (
        (display next-command-args)(newline)
        (display next-command-name) (newline)))))
