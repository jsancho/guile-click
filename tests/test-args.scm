;;; Click --- Command Line Interface Creation Kit for GNU Guile
;;; Copyright © 2021 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of Click.
;;;
;;; Click is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Click is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Click.  If not, see <http://www.gnu.org/licenses/>.


(define-module (tests test-args)
  #:use-module (srfi srfi-64)
  #:use-module (click args))


(test-begin "test-args")

(define option-spec
  '((count (single-char #\c) (default 1))
    (name  (prompt "Your name"))
    (help  (flag #t))))

(test-error #t (parse-args '("program" "--count") option-spec))

(test-error #t (parse-args '("program" "-x") option-spec))

(test-equal (parse-args '("program" "-c" "2" "--name" "joe") option-spec)
            '((()) (name . "joe") (count . "2")))

(test-equal (parse-args '("program" "-c" "2" "command" "-x") option-spec)
            '((() "command" "-x") (count . "2")))

(test-equal (get-values option-spec '((()) (name . "joe") (count . "2")))
            '((count . 2) (name . "joe") (help . #f)))
              
(test-end "test-args")
