;;; Click --- Command Line Interface Creation Kit for GNU Guile
;;; Copyright © 2021 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of Click.
;;;
;;; Click is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Click is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Click.  If not, see <http://www.gnu.org/licenses/>.


(define-module (click util)
  #:use-module (click constant)
  #:export (getopt-long-option-spec
            option-property
            option-type))


(define (getopt-long-option-spec option-spec)
  "Transform click option spec into getopt-long format"
  (map (lambda (option)
         (cons (car option)
               (cons (list 'value (not (option-property option 'flag)))
                     (filter (lambda (property)
                               (not (memq (car property) CLICK_PROPERTIES)))
                             (cdr option)))))
       option-spec))


(define* (option-property option property-name #:optional (default #f))
  "Return the option property with a given name"
  (let ((property (assq property-name (cdr option))))
    (if property
        (cadr property)
        default)))


;; Types
(define TYPE-TEXT
  `((description . "TEXT")
    (convert . ,identity)))
(define TYPE-INTEGER
  `((description . "INTEGER")
    (convert . ,string->number)))
(define TYPE-NUMBER
  `((description . "NUMBER")
    (convert . ,string->number)))

(define (option-type option)
  "Return allowed type for the value in the option"
  (let ((default (option-property option 'default)))
    (cond ((not default)
           TYPE-TEXT)
          ((integer? default)
           TYPE-INTEGER)
          ((number? default)
           TYPE-NUMBER)
          (else
           TYPE-TEXT))))
