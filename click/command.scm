;;; Click --- Command Line Interface Creation Kit for GNU Guile
;;; Copyright © 2021 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of Click.
;;;
;;; Click is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Click is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Click.  If not, see <http://www.gnu.org/licenses/>.


(define-module (click command)
  #:export (command?
            command-commands
            command-help-text
            command-name
            command-option-spec
            command-procedure
            group?
            make-command
            set-command-click-manager!))


;; Command VTable
(define <command-vtable> (make-struct/no-tail <applicable-struct-vtable> 'pwpwpwpwpwpw))

(define (command-printer struct port)
  (let ((type (if (group? struct) "Group" "Command"))
        (name (command-name struct)))
    (if name
        (format port "<~a ~a>" type name)
        (format port "<~a>" type))))

(struct-set! <command-vtable> vtable-index-printer command-printer)


;; Command API
(define (make-command name option-spec help-text procedure commands)
  (make-struct/no-tail <command-vtable> #f name option-spec help-text procedure commands))

(define (set-command-click-manager! command click-manager)
  (struct-set! command 0 click-manager))

(define (command-name command)
  (struct-ref command 1))

(define (command-option-spec command)
  (struct-ref command 2))

(define (command-help-text command)
  (struct-ref command 3))

(define (command-procedure command)
  (struct-ref command 4))

(define (command-commands command)
  (struct-ref command 5))

(define (command? command)
  (and (equal? (struct-vtable command) <command-vtable>)
       (null? (command-commands command))))

(define (group? command)
  (and (equal? (struct-vtable command) <command-vtable>)
       (not (null? (command-commands command)))))
