;;; Click --- Command Line Interface Creation Kit for GNU Guile
;;; Copyright © 2021 Javier Sancho <jsf@jsancho.org>
;;;
;;; This file is part of Click.
;;;
;;; Click is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Click is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Click.  If not, see <http://www.gnu.org/licenses/>.


(define-module (click help)
  #:use-module (ice-9 format)
  #:use-module (click command)
  #:use-module (click util)
  #:export (display-error
            display-help))


(define (get-type-description option)
  "Return allowed type for the value in the option"
  (assoc-ref (option-type option) 'description))


(define* (get-help-text procedure #:optional (help-text ""))
  "Return the title for the help message"
  (cond ((> (string-length help-text) 0)
         help-text)
        ((procedure? procedure)
         (let ((doc-text (procedure-documentation procedure)))
           (and (string? doc-text)
                (> (string-length doc-text) 0)
                doc-text)))
        (else
         help-text)))


(define (get-help-text command)
  "Return the title for the help message"
  (let ((procedure (command-procedure command))
        (help-text (command-help-text command)))
    (cond ((> (string-length help-text) 0)
           help-text)
          ((procedure? procedure)
           (let ((doc-text (procedure-documentation procedure)))
             (and (string? doc-text)
                  (> (string-length doc-text) 0)
                  doc-text)))
          (else
           help-text))))


(define (display-options options)
  "Display options in tabular way"
  (let ((sep (+ 2 (apply max (map (lambda (option)
                                    (string-length (car option)))
                                  options)))))
    (for-each (lambda (option)
                (format #t
                        "  ~a~v_~a~%"
                        (car option)
                        (- sep (string-length (car option)))
                        (cdr option)))
              options)))


(define* (display-help program-name command)
  "Display help message"

  ;; Usage
  (format #t "Usage: ~a [OPTIONS]" program-name)
  (when (group? command)
    (format #t " COMMAND [ARGS]..."))
  (format #t "~%~%")

  ;; Title
  (let ((title (get-help-text command)))
    (when title
      (format #t "  ~a~%~%" title)))

  ;; Options
  (format #t "Options:~%")
  (let ((options (map (lambda (option)
                        (cons (format #f
                                      "--~a~a~a"
                                      (car option)
                                      (let ((single-char
                                             (option-property option 'single-char)))
                                        (if single-char
                                            (format #f ", -~a" single-char)
                                            ""))
                                      (let ((value
                                             (not (option-property option 'flag))))
                                        (if value
                                            (format #f " ~a" (get-type-description option))
                                            "")))
                              (option-property option 'help "")))
                      (command-option-spec command))))
    (display-options options))

  ;; Commands
  (when (group? command)
    (format #t "~%Commands:~%")
    (let ((options (map (lambda (command)
                          (cons (or (command-name command) "")
                                (or (get-help-text command) "")))
                        (command-commands command))))
      (display-options options))))


(define (display-error wrong-option)
  (format #t "Usage: ~a [OPTIONS]
Try '~a --help' for help.

Error: No such option: ~a
" (program-name) (program-name) wrong-option))
